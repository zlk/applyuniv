'use strict';

angular.module('mainModule', [])
.controller('mainController', ['$scope', '$location', function($scope, $location){

}]);                                    
                                        
/* create root module westudentsApp */
angular.module('westudentsApp', ['underscore', 'pascalprecht.translate', 'rzModule',
                                 'ngResource', 'ngRoute', 'ngCookies', 'ngSanitize', 'ngTouch', 'ngAnimate',
	                             'ui.bootstrap'
                           ])
//-------------------------------------------------------------------------------
// Get executed during the provider registrations and configuration phase. 
// Only providers and constants can be injected into configuration blocks.
//-------------------------------------------------------------------------------
.config(['$routeProvider', '$translateProvider',  function($routeProvider, $translateProvider){
	
    $routeProvider
      .when('/', {
        templateUrl: '/views/home.html',
        controller: 'HomeController'
      })
      .otherwise({
    	  	redirectTo: '/'
      });
    
    // Translation
    //$translateProvider.translations('en_US', en_US );  
    //$translateProvider.translations('zh_CN', zh_CN );
    
    //$translateProvider.preferredLanguage('en_US');
}]);

